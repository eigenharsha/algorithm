String Algorithms
==================
## Knuth-Morris-Pratt

[Knuth-Morris-Pratt C++ Source Code : Ideone](http://ideone.com/9A6nCf)

The fundamental string searching (matching) problem is defined as follows: given two strings – a text and a pattern, determine whether the pattern appears in the text. The problem is also known as “the needle in a haystack problem.”

The “Naive” Method
Its idea is straightforward — for every position in the text, consider it a starting position of the pattern and see if you get a match.

```c++
function brute_force(text[], pattern[]) 
{
  // let n be the size of the text and m the size of the
  // pattern

  for(i = 0; i < n; i++) {
    for(j = 0; j < m && i + j < n; j++) 
      if(text[i + j] != pattern[j]) break;
      // mismatch found, break the inner loop
    if(j == m) // match found
  }
}
```
----------------

### The Partial Match Table

The key to KMP, of course, is the partial match table. The main obstacle between me and understanding KMP was the fact that I didn’t quite fully grasp what the values in the partial match table really meant. I will now try to explain them in the simplest words possible.

Here’s the partial match table for the pattern “abababca”:
```ruby
char:  | a | b | a | b | a | b | c | a |
index: | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 
value: | 0 | 0 | 1 | 2 | 3 | 4 | 0 | 1 |
```

If I have an eight-character pattern (let’s say “abababca” for the duration of this example), my partial match table will have eight cells. If I’m looking at the eighth and last cell in the table, I’m interested in the entire pattern (“abababca”). If I’m looking at the seventh cell in the table, I’m only interested in the first seven characters in the pattern (“abababc”); the eighth one (“a”) is irrelevant, and can go fall off a building or something. If I’m looking at the sixth cell of the in the table… you get the idea. Notice that I haven’t talked about what each cell _means_ yet, but just what it’s referring to.

Now, in order to talk about the meaning, we need to know about **proper prefixes** and **proper suffixes**.

**Proper prefix**: All the characters in a string, with one or more cut off the end. 
**a b a b a b c a**
as a pattern, and let’s list all its prefixes:

```c++
0 a
1 a b
2 a b a 
3 a b a b 
4 a b a b a 
5 a b a b a b 
6 a b a b a b c 
```
**Proper suffix**: All the characters in a string, with one or more cut off the beginning. 
**a b a b a b c a**
as a pattern, and let’s list all its suffixes:
```c++
0 b a b a b c a
1 a b a b c a 
2 b a b c a 
3 a b c a 
4 b c a 
5 c a
6 a
```

With this in mind, I can now give the one-sentence meaning of the values in the partial match table:

**The length of the longest proper prefix in the (sub)pattern that matches a proper suffix in the same (sub)pattern.**

Let’s examine what I mean by that. Say we’re looking in the third cell. As you’ll remember from above, this means we’re only interested in the first three characters (“aba”). In “aba”, there are two proper prefixes (“a” and “ab”) and two proper suffixes (“a” and “ba”). The proper prefix “ab” does not match either of the two proper suffixes. However, the proper prefix “a” matches the proper suffix “a”. Thus, **the length of the longest proper prefix that matches a proper suffix**, in this case, is 1.

Let’s try it for cell four. Here, we’re interested in the first four characters (“abab”). We have three proper prefixes (“a”, “ab”, and “aba”) and three proper suffixes (“b”, “ab”, and “bab”). This time, “ab” is in both, and is two characters long, so cell four gets value 2.

Just because it’s an interesting example, let’s also try it for cell five, which concerns “ababa”. We have four proper prefixes (“a”, “ab”, “aba”, and “abab”) and four proper suffixes (“a”, “ba”, “aba”, and “baba”). Now, we have two matches: “a” and “aba” are both proper prefixes and proper suffixes. Since “aba” is longer than “a”, it wins, and cell five gets value 3.

Let’s skip ahead to cell seven (the second-to-last cell), which is concerned with the pattern “abababc”. Even without enumerating all the proper prefixes and suffixes, it should be obvious that there aren’t going to be any matches; all the suffixes will end with the letter “c”, and none of the prefixes will. Since there are no matches, cell seven gets 0.

Finally, let’s look at cell eight, which is concerned with the entire pattern (“abababca”). Since they both start and end with “a”, we know the value will be at least 1\. However, that’s where it ends; at lengths two and up, all the suffixes contain a c, while only the last prefix (“abababc”) does. This seven-character prefix does not match the seven-character suffix (“bababca”), so cell eight gets 1.

In order to build the KMP automaton (or the so called KMP **failure function**) we have to initialize an integer array F[]. The indexes (from 0 to m – the length of the pattern) represent the numbers under which the consecutive prefixes of the pattern are listed in our “list of prefixes” above. Under each index is a “pointer” – that identifies the index of the longest proper suffix, which is at the same time a prefix of the given string (or in other words F[i] is the index of next best partial match for the string under index i). In our case (the string **a b a b a b c a**) the array F[] will look as follows:

```ruby
pattern: | a | b | a | b | a | b | c | a |
index:   | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 |
F(i):    | 0 | 0 | 1 | 2 | 3 | 4 | 0 | 1 |
```
Notice that after initialization F[i] contains information not only about the largest next partial match for the string under index i but also about every partial match of it. F[i] is the first best partial match, F[F[i]] – is the second best, F[F[F[i]]] – the third, and so on. Using this information we can calculate F[i] if we know the values F[k] for all k < i. The best next partial match of string i will be the largest partial match of string i – 1 whose character that “expands” it is equal to the last character of string i. So all we need to do is to check every partial match of string i – 1 in descending order of length and see if the last character of string i “expands” the match at this level. If no partial match can be “expanded” than F[i] is the empty string. Otherwise F[i] is the largest “expanded” partial match (after its “expansion”).

In terms of pseudocode the initialization of the array F[] (the “failure function”) may look like this:

```c++
// Pay attention! 
// the prefix under index i in the table above is 
// is the string from pattern[0] to pattern[i - 1] 
// inclusive, so the last character of the string under 
// index i is pattern[i - 1]

void build_failure_function(char pattern[], int m, F[]) {
    // m be the length of the pattern 
    F[0] = 0; // always true

    for (int i = 1; i <= m; i++) {
        // j is the index of the largest next partial match 
        // (the largest suffix/prefix) of the string under  
        // index i - 1
        int j = F[i - 1];
        for (; ; ) {
            // check to see if the last character of string i - 
            // - pattern[i - 1] "expands" the current "candidate"
            // best partial match - the prefix under index j
            if (pattern[j] == pattern[i]) {
                F[i] = j + 1;
                break;
            }
            // if we cannot "expand" even the empty string
            if (j == 0) {
                F[i] = 0;
                break;
            }
            // else go to the next best "candidate" partial match
            j = F[j];
        }
    }
	/***
    for (int i = 0; i < m + 1; i++) {
        cout << F[i] << " " << endl;
    }
    */
}
```

## How to use the Partial Match Table

We can use the values in the partial match table to skip ahead (rather than redoing unnecessary old comparisons) when we find partial matches. The formula works like this:

_If a partial match of length **partial_match_length** is found and `table[partial_match_length] > 1`, we may skip ahead `partial_match_length - table[partial_match_length - 1]` characters._

Let’s say we’re matching the pattern “abababca” against the text “bacbababaabcbab”. Here’s our partial match table again for easy reference:

```ruby
char:  | a | b | a | b | a | b | c | a |
index: | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 
value: | 0 | 0 | 1 | 2 | 3 | 4 | 0 | 1 |
```

```c++
bacbababaabcbab
 |
 abababca
```

This is a partial_match_length of 1\. The value at `table[partial_match_length - 1]` (or `table[0]`) is 0, so we don’t get to skip ahead any. The next partial match we get is here:

```c++
bacbababaabcbab
    |||||
    abababca
```

This is a partial_match_length of 5\. The value at `table[partial_match_length - 1]` (or `table[4]`) is 3\. That means we get to skip ahead `partial_match_length - table[partial_match_length - 1]` (or `5 - table[4]` or `5 - 3` or `2`) characters:

```c++
// x denotes a skip

bacbababaabcbab
    xx|||
      abababca
```
This is a partial_match_length of 3\. The value at `table[partial_match_length - 1]` (or `table[2]`) is 1\. That means we get to skip ahead `partial_match_length - table[partial_match_length - 1]` (or `3 - table[2]` or `3 - 1` or `2`) characters:

```c++
// x denotes a skip

bacbababaabcbab
      xx|
        abababca
```

At this point, our pattern is longer than the remaining characters in the text, so we know there’s no match.

```c++
function Knuth_Morris_Pratt(text[], pattern[], int n, int m) {
    // let n be the size of the text, m the 
    // size of the pattern, and F[] - the
    // "failure function"
    
    int F[m] = {0}
    build_failure_function(pattern[], F, m);

    int i = 0; // the first character of the text
    int j = 0; // the initial state of the automaton is
        // the empty string

    for (; ; ) {
        if (i == n) break; // we reached the end of the text

        // if the current character of the text "expands" the
        // current match 
        if (text[i] == pattern[j]) {
            i++; // change the state of the automaton
            j++; // get the next character from the text
            if (j == m) // match found
            {
                cout<< "Found pattern at index " << i-j << endl;
                j = F[j-1];
            }
        }

        // if the current state is not zero (we have not
        // reached the empty string yet) we try to
        // "expand" the next best (largest) match
        else if (j > 0) j = F[j-1];

            // if we reached the empty string and failed to
            // "expand" even it; we go to the next 
            // character from the text, the state of the
            // automaton remains zero
        else i++;
    }
}
```

## Conclusion

So there you have it. Like I promised before, it’s no exhaustive explanation or formal proof of KMP; it’s a walk through my brain, with the parts I found confusing spelled out in extreme detail. If you have any questions or notice something I messed up, please leave a comment; maybe we’ll all learn something.